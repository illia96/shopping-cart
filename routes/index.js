var express = require('express');
var router = express.Router();
var csrf = require('csurf');
var fixer = require('fixer-api');
var Cart  = require('../models/cart');

var Product = require('../models/product');
var Category = require('../models/category');
var Order = require('../models/order');
var csrfProtection = csrf();
router.use(csrfProtection);

var cur;
var sign;

/* GET home page. */
router.get('/', function (req, res, next) {
    var succesMsg = req.flash('success')[0];
    Category.find(function (err, doc) {
        req.session.oldUrlCart = req.url;
        res.render('shop/index', {
            title: 'Shopping Cart',
            succesMsg: succesMsg,
            noSuccesMsg: !succesMsg,
            csrfToken: req.csrfToken(),
            categories: doc,
        });
    });
});

router.get('/shop/mens', function (req, res, next) {
  var succesMsg = req.flash('success')[0];
  Category.find({id:'mens'},function (err, doc) {
      req.session.oldUrlCart = req.url;
      res.render('shop/mens', {
          title: 'Shopping Cart',
          succesMsg: succesMsg,
          noSuccesMsg: !succesMsg,
          csrfToken: req.csrfToken(),
          categories: doc,
      });
  });
});

router.get('/shop/womens', function (req, res, next) {
  var succesMsg = req.flash('success')[0];
  Category.find({id:'womens'},function (err, doc) {
      req.session.oldUrlCart = req.url;
      res.render('shop/womens', {
          title: 'Shopping Cart',
          succesMsg: succesMsg,
          noSuccesMsg: !succesMsg,
          csrfToken: req.csrfToken(),
          categories: doc,
      });
  });
});

router.get('/shop/mens-clothing', function (req, res, next) {
  var succesMsg = req.flash('success')[0];
  Category.find({id:'mens'},function (err, doc) {
      req.session.oldUrlCart = req.url;
              var secondCat=[];
              doc[0].categories.forEach(function (val) {
                 if(val.id === 'mens-clothing'){
                  secondCat = val.categories;
                 }
          });
      res.render('shop/mens-clothing', {
          title: 'Shopping Cart',
          succesMsg: succesMsg,
          noSuccesMsg: !succesMsg,
          csrfToken: req.csrfToken(),
          categories: secondCat,
      });
  });
});

router.get('/shop/mens-accessories', function (req, res, next) {
  var succesMsg = req.flash('success')[0];
  Category.find({id:'mens'},function (err, doc) {
      req.session.oldUrlCart = req.url;
              var secondCat=[];
              doc[0].categories.forEach(function (val) {
                 if(val.id === 'mens-accessories'){
                  secondCat = val.categories;
                 }
          });
      res.render('shop/mens-accessories', {
          title: 'Shopping Cart',
          succesMsg: succesMsg,
          noSuccesMsg: !succesMsg,
          csrfToken: req.csrfToken(),
          categories: secondCat,
      });
  });
});

router.get('/shop/womens-clothing', function (req, res, next) {
  var succesMsg = req.flash('success')[0];
  Category.find({id:'womens'},function (err, doc) {
      req.session.oldUrlCart = req.url;
      var secondCat=[];
      doc[0].categories.forEach(function (val) {
         if(val.id === 'womens-clothing'){
          secondCat = val.categories;
         }
  });
      res.render('shop/womens-clothing', {
          title: 'Shopping Cart',
          succesMsg: succesMsg,
          noSuccesMsg: !succesMsg,
          csrfToken: req.csrfToken(),
          categories: secondCat,
      });
  });
});

router.get('/shop/womens-jewelry', function (req, res, next) {
  var succesMsg = req.flash('success')[0];
  Category.find({id:'womens'},function (err, doc) {
      req.session.oldUrlCart = req.url;
      var secondCat=[];
      doc[0].categories.forEach(function (val) {
         if(val.id === 'womens-jewelry'){
          secondCat = val.categories;
         }
  });
      res.render('shop/womens-jewelry', {
          title: 'Shopping Cart',
          succesMsg: succesMsg,
          noSuccesMsg: !succesMsg,
          csrfToken: req.csrfToken(),
          categories: secondCat,
      });
  });
});

router.get('/shop/womens-accessories', function (req, res, next) {
  var succesMsg = req.flash('success')[0];
  Category.find({id:'womens'},function (err, doc) {
      req.session.oldUrlCart = req.url;
      var secondCat=[];
      doc[0].categories.forEach(function (val) {
         if(val.id === 'womens-accessories'){
          secondCat = val.categories;
         }
  });
      res.render('shop/womens-accessories', {
          title: 'Shopping Cart',
          succesMsg: succesMsg,
          noSuccesMsg: !succesMsg,
          csrfToken: req.csrfToken(),
          categories: secondCat,
      });
  });
});

router.get('/products/:id', function (req, res, next) {
    var succesMsg = req.flash('success')[0];
    Product.find({primary_category_id: req.params.id}, function (err, docs) {
        var productChumks = [];
        var chunkSize = 3;
        for (var i = 0; i < docs.length; i += chunkSize) {
            productChumks.push(docs.slice(i, i + chunkSize));
        }
        fixer.latest({access_key: '3a90f49f66c40c0abe459df4dbac3bfe'})
            .then(function (data) {
                if (req.session.currency === "USD") {
                    cur = 1;
                    sign = '$';
                } else if (req.session.currency === "EUR") {
                    cur = 1 / 1.141965;
                    sign = '€';
                }
                else if (req.session.currency === "GBP") {
                    cur = 1 / 1.312353;
                    sign = '£';
                }
                else if (req.session.currency === "RON") {
                    cur = 1 * 4.08299921;
                    sign = 'L';
                }
                else if (req.session.currency === "UAH") {
                    cur = 1 * 27.9782888;
                    sign = '₴';
                }
                productChumks.forEach(function (products) {
                    products.forEach(function (product) {
                        product.price = product.price * cur;
                        product.price = product.price.toFixed(2);
                        product.cur = sign;
                        product.imagePath = product.image_groups[0].images[0].link;
                    });
                });
            }).then(function () {
            var breadcrumbs = req.params.id.split('-');
            req.session.oldUrlCart = req.url;
            res.render('shop/products', {
                title: 'Shopping Cart',
                products: productChumks,
                succesMsg: succesMsg,
                noSuccesMsg: !succesMsg,
                csrfToken: req.csrfToken(),
                breadcrumbs: breadcrumbs[0] + "-" + breadcrumbs[1],
                breadcrumbs_name: breadcrumbs[0] + " " + breadcrumbs[1],
                second_breadcrumbs_name: breadcrumbs[2],

            });
        });
    });
});

router.get('/product/:id', function (req, res, next) {
    Product.findById(req.params.id, function (err, docs) {
        var larg = [], smal = [];
        docs.image_groups.forEach(function (value) {
            if (value.view_type === "large") {
                larg = value.images;
            }
            if (value.view_type === "small") {
                smal = value.images;
            }
        });
        fixer.latest({access_key: '3a90f49f66c40c0abe459df4dbac3bfe'})
            .then(function (data) {
                if (req.session.currency === "USD") {
                    cur = 1;
                    sign = '$';
                } else if (req.session.currency === "EUR") {
                    cur = 1 / data.rates['USD'];
                    sign = '€';
                }
                else if (req.session.currency === "GBP") {
                  cur = 1 / 1.312353;
                  sign = '£';
                }
                else if (req.session.currency === "RON") {
                  cur = 1 * 4.08299921;
                  sign = 'L';
                }
                else if (req.session.currency === "UAH") {
                    cur = 1 * 27.9782888;
                    sign = '₴';
                }
                docs.price = (docs.price * cur).toFixed(2);
                docs.cur = sign;
            }).then(function () {
            var breadcrumbs = docs.primary_category_id.split('-');
            req.session.oldUrlCart = req.url;
            res.render('shop/product', {
                title: 'Shopping Cart',
                product: docs,
                smal: smal,
                large: larg[0],
                csrfToken: req.csrfToken(),
                breadcrumbs: breadcrumbs[0] + "-" + breadcrumbs[1],
                breadcrumbs_name: breadcrumbs[0] + " " + breadcrumbs[1],
                second_breadcrumbs_name: breadcrumbs[2],
            });
        });
    });
});

router.post('/add-to-cart', function (req, res, next) {
    var productId = req.body._id;
    var cart = new Cart(req.session.cart ? req.session.cart : {});

    Product.findById(productId, function (err, product) {
        if (err) {
            return res.redirect('/');
        }
        cart.add(product, product.id, req.body.color, req.body.size, req.body.width);
        req.session.cart = cart;
        if (req.session.oldUrlCart) {
            var oldUrl = req.session.oldUrlCart;
            req.session.oldUrlCart = null;
            res.redirect(oldUrl);
        } else {
            res.redirect('/');
        }

    })
});

router.get('/shopping-cart', function (req, res, next) {
    if (!req.session.cart) {
        return res.render('shop/shopping-cart', {products: null});
    }
    var cart = new Cart(req.session.cart);
    req.session.oldUrlCart = req.url;
    res.render('shop/shopping-cart', {
        products: cart.generateArray(),
        totalPrice: cart.totalPrice,
        csrfToken: req.csrfToken()
    })
});


router.get('/reduce/:id', function (req, res, next) {
    var productId = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart : {});

    cart.reduceByOne(productId);
    req.session.cart = cart;
    res.redirect('/shopping-cart');
});

router.get('/remove/:id', function (req, res, next) {
    var productId = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart : {});

    cart.removeItem(productId);
    req.session.cart = cart;
    res.redirect('/shopping-cart');
});

router.get('/checkout', isLoggedIn, function (req, res, next) {
    if (!req.session.cart) {
        return res.redirect('/shopping-cart')
    }
    var error = req.flash('error')[0];
    var cart = new Cart(req.session.cart);
    res.render('shop/checkout', {total: cart.totalPrice, errmsg: error, csrfToken: req.csrfToken()});
});

router.post('/checkout', isLoggedIn, function (req, res, next) {
    if (!req.session.cart) {
        return res.redirect('/shopping-cart')
    }
    var cart = new Cart(req.session.cart);
    var stripe = require("stripe")("sk_test_NrOwaWVVqhTYekxThUY2QbK9");

    stripe.charges.create({
        amount: cart.totalPrice * 100,
        currency: "usd",
        source: req.body.stripeToken,
        description: "Charge from " + req.user.email
    }, function (err, charge) {
        if (err) {
            req.flash('error', err.message);
            return res.redirect('/checkout');
        }
        var order = new Order({
            user: req.user,
            cart: cart,
            paymentID: charge.id
        });
        order.save(function (err, result) {
            req.flash('success', 'Successufully bought product!');
            req.session.cart = null;
            res.redirect('/');
        });
    });
});

router.get('/usd', function (req, res, next) {
    req.session.currency = "USD";
    if (req.session.oldUrlCart) {
        var oldUrl = req.session.oldUrlCart;
        req.session.oldUrlCart = null;
        res.redirect(oldUrl);
    } else {
        res.redirect('/');
    }
});
router.get('/eur', function (req, res, next) {
    req.session.currency = "EUR";
    if (req.session.oldUrlCart) {
        var oldUrl = req.session.oldUrlCart;
        req.session.oldUrlCart = null;
        res.redirect(oldUrl);
    } else {
        res.redirect('/');
    }
});
router.get('/gbp', function (req, res, next) {
    req.session.currency = "GBP";
    if (req.session.oldUrlCart) {
        var oldUrl = req.session.oldUrlCart;
        req.session.oldUrlCart = null;
        res.redirect(oldUrl);
    } else {
        res.redirect('/');
    }
});
router.get('/ron', function (req, res, next) {
    req.session.currency = "RON";
    if (req.session.oldUrlCart) {
        var oldUrl = req.session.oldUrlCart;
        req.session.oldUrlCart = null;
        res.redirect(oldUrl);
    } else {
        res.redirect('/');
    }
});
router.get('/uah', function (req, res, next) {
    req.session.currency = "UAH";
    if (req.session.oldUrlCart) {
        var oldUrl = req.session.oldUrlCart;
        req.session.oldUrlCart = null;
        res.redirect(oldUrl);
    } else {
        res.redirect('/');
    }
});

router.get("/search", function (req, res, next) {
    var succesMsg = req.flash('success')[0];
    Product.find({name: {$regex: req.query.string, $options: "i"}}, function (err, docs) {
        var productChumks = [];
        var chunkSize = 3;
        for (var i = 0; i < docs.length; i += chunkSize) {
            productChumks.push(docs.slice(i, i + chunkSize));
        }
        fixer.latest({access_key: '3a90f49f66c40c0abe459df4dbac3bfe'})
        .then(function (data) {
            if (req.session.currency === "USD") {
                cur = 1;
                sign = '$';
            } else if (req.session.currency === "EUR") {
                cur = 1 / data.rates['USD'];
                sign = '€';
            }
            else if (req.session.currency === "GBP") {
              cur = 1 / 1.312353;
              sign = '£';
            }
            else if (req.session.currency === "RON") {
              cur = 1 * 4.08299921;
              sign = 'L';
            }
            else if (req.session.currency === "UAH") {
                cur = 1 * 27.9782888;
                sign = '₴';
            }
                productChumks.forEach(function (products) {
                    products.forEach(function (product) {
                        product.price = product.price * cur;
                        product.price = product.price.toFixed(2);
                        product.cur = sign;
                        product.imagePath = product.image_groups[0].images[0].link;
                    });
                });
            }).then(function () {
            req.session.oldUrlCart = req.url;
            res.render('shop/products', {
                title: 'Shopping Cart',
                products: productChumks,
                succesMsg: succesMsg,
                noSuccesMsg: !succesMsg,
                csrfToken: req.csrfToken(),
                breadcrumbs_name: "Search"
            });
        });
    });
});

module.exports = router;

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    req.session.oldUrl = req.url;
    res.redirect('/user/signin');
}
