var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
  price_max: {type: Number},
  page_description: {type: String},
  page_title: {type: String},
  name: {type: String},
  variation_attributes : {type:Array},
  currency: {type: String},
  primary_category_id: {type: String},
  short_description: {type: String},
  long_description: {type: String},
  image_groups: {type: Array},
  imagePath: {type: String, required: true},
  title: {type: String, required: true},
  description: {type: String, required: true},
  price: {type: Number, required: true},
  category: {type: Schema.Types.ObjectId, ref: 'Category'}
});

module.exports = mongoose.model('Product',schema);
