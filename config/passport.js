var passport  = require('passport');
var User  = require('../models/user');
var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
passport.serializeUser(function(user, done) {
  done(null,  user.id);
});

passport.deserializeUser(function(id, done) {
  User.findById(id, function(err, user) {
    done(err, user);
  });
});

passport.use('local.signup',  new LocalStrategy({
  usernameField:  'email',
  passwordField:  'password',
  passReqToCallback:  true
},  function(req, email,  password, done) {
  req.checkBody('email',  'Invalid email').notEmpty().isEmail();
  req.checkBody('password',  'Invalid password').notEmpty().isLength({min:6});
  var errors  = req.validationErrors();
  if (errors) {
    var messages  = [];
    errors.forEach(function(error){
      messages.push(error.msg);
    });
    return  done(null,  false,  req.flash('error',  messages));
  }
  User.findOne({'email':  email}, function(err, user) {
    if (err) {
      return done(err);
    }
    if (user) {
      return done(null, false,  {message: 'Email is already in use.'});
    }
    var newUser = new User();
    newUser.email = email;
    newUser.password  = newUser.encryptPassword(password);
    newUser.save(function(err,  result) {
      if (err) {
        return done(err);
      }
      return  done(null,  newUser);
    });
  });
}));

passport.use('local.signin',  new LocalStrategy({
  usernameField:  'email',
  passwordField:  'password',
  passReqToCallback:  true
},  function(req, email,  password, done) {
  req.checkBody('email',  'Invalid email').notEmpty().isEmail();
  req.checkBody('password',  'Invalid password').notEmpty();
  var errors  = req.validationErrors();
  if (errors) {
    var messages  = [];
    errors.forEach(function(error){
      messages.push(error.msg);
    });
    return  done(null,  false,  req.flash('error',  messages));
  }
  User.findOne({'email':  email}, function(err, user) {
    if (err) {
      return done(err);
    }
    if (!user) {
      return done(null, false,  {message: 'No user found.'});
    }
    if (!user.validPassword(password)) {
      return done(null, false,  {message: 'Wrong password.'});
    }
return done(null, user);
  });
}));

passport.use(new FacebookStrategy({
    clientID: 281722432683238,
    clientSecret: '9b67c222cde9233695b08a1425110249',
    callbackURL: "http://localhost:3000/user/facebook/callback",
    profileFields: ['id', 'email']
},
  // facebook will send back the tokens and profile
function (accessToken, refreshToken, profile, done) {
  // asynchronous
  //  process.nextTick(function() {
     // find the user in the database based on their facebook id
    User.findOne({'email': profile.emails[0].value}, function (err, user) {

        // if there is an error, stop everything and return that
        // ie an error connecting to the database
        if (err) {
            return done(err);
                 // if the user is found, then log them in
        }
        if (user) {
            return done(null, user); // user found, return that user
        }
        else {
            // if there is no user found with that facebook id, create them
            var newUser = new User();
              // set all of the facebook information in our user model
            newUser.email = profile.emails[0].value;
            newUser.password = 'from facebook';
                // save our user to the database
            newUser.save(function (err,) {
                if (err)
                  throw err;
              // if successful, return the new user
                return done(null, newUser);

            });
        }
    });

}));

passport.use(new GoogleStrategy({
    clientID: '775771536384-ap7rrosbqqmf184aj742uv7ljo9s9qcl.apps.googleusercontent.com',
    clientSecret: 'AA2e1mh70f7KvK3tcts2jQEC',
    callbackURL: "http://localhost:3000/user/google/callback",
    profileFields: ['id', 'email']
}, function (accessToken, refreshToken, profile, done) {
  // asynchronous
  //  process.nextTick(function() {
     // find the user in the database based on their facebook id
    User.findOne({'email': profile.emails[0].value}, function (err, user) {
      // if there is an error, stop everything and return that
      // ie an error connecting to the database
        if (err) {
            return done(err);
        // if the user is found, then log them in
        }
        if (user) {
            return done(null, user);
        }
        else {
            // if there is no user found with that facebook id, create them
            var newUser = new User();
              // set all of the facebook information in our user model
            newUser.email = profile.emails[0].value;
            newUser.password = 'from google';
          // save our user to the database
            newUser.save(function (err,) {
                if (err) throw err;
            // if successful, return the new user
                return done(null, newUser);

            });
        }
    });

}));
