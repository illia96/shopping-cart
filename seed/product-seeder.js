var Category = require('../models/category');

var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/shop', {useNewUrlParser: true});

var cat = new Category({
  categories: [
              {
                  categories: [
                      {
                          id: "mens-clothing-suits",
                          image: "categories/mens-clothing-suits.jpg",
                          name: "Suits",
                          page_description: "Shop Men's suits for business or pleasure. Enjoy from a variety of different styles and cuts.",
                          page_title: "Mens Suits for Business and Casual",
                          parent_category_id: "mens-clothing",
                          c_showInMenu: true
                      },
                      {
                          id: "mens-clothing-jackets",
                          image: "categories/mens-clothing-sportscoats.png",
                          name: "Jackets & Coats",
                          page_description: "Shop Men's Jackets, Coats & Outerwear. Classic outdoor-tested garments with traditional styling details that provide comfort, insulation and ease of movement, whatever the weather.",
                          page_title: "Men's Jackets Including Jackets & Blazzers",
                          parent_category_id: "mens-clothing",
                          c_showInMenu: true
                      },
                      {
                          id: "mens-clothing-dress-shirts",
                          image: "categories/mens-clothing-dress-shirts.jpg",
                          name: "Dress Shirts",
                          page_description: "Shop Men's dress shirts in a variety of colors and styles including striped, button down, non-iron & more",
                          page_title: "Men's Dress Shirts including Striped, Button Down, Non-Iron & More",
                          parent_category_id: "mens-clothing",
                          c_showInMenu: true
                      },
                      {
                          id: "mens-clothing-shorts",
                          image: "categories/mens-clothing-shorts.png",
                          name: "Shorts",
                          page_description: "Shop Men's spring shorts in cotton. Variety of different fits.",
                          page_title: "Men's Spring Shorts",
                          parent_category_id: "mens-clothing",
                          c_showInMenu: true
                      },
                      {
                          id: "mens-clothing-pants",
                          image: "categories/mens-clothing-pants.png",
                          name: "Pants",
                          page_description: "Shop Men's Trousers. Practical, easy-to-wear styles wherever you're headed. Check out famous rugged, long-lasting trousers, jeans, cargo pants and more.",
                          page_title: "Men's Pants Including Khakis, Cargos, Trousers, Jeans & More",
                          parent_category_id: "mens-clothing",
                          c_showInMenu: true
                      }
                  ],
                  id: "mens-clothing",
                  image: "categories/mens-clothing-accessories.jpg",
                  name: "Clothing",
                  page_description: "Shop Men's Clothing. Relaxed, timeless classics you can rely on; from denim to corduroys and sweaters to shirts. Huge range of contemporary colours and eco-aware designs: great casualwear.",
                  page_title: "Mens Clothing Including Suits, Tops, Bottoms & More",
                  parent_category_id: "mens",
                  c_showInMenu: true
              },
              {
                  categories: [
                      {
                          id: "mens-accessories-ties",
                          image: "categories/mens-accessories-ties.png",
                          name: "Ties",
                          page_description: "Shop Mens's Ties for all occasions including business or casual.",
                          page_title: "Men's Casual and Business Ties",
                          parent_category_id: "mens-accessories",
                          c_showInMenu: true
                      },
                      {
                          id: "mens-accessories-gloves",
                          name: "Gloves",
                          page_description: "Shop Men'sGloves. Versatile, commuter, boot, oxford, deer and resolve gloves. All with famous long-lasting quality.",
                          page_title: "Men's Gloves",
                          parent_category_id: "mens-accessories",
                          c_showInMenu: true
                      },
                      {
                          id: "mens-accessories-luggage",
                          image: "categories/mens-accessories-luggage.jpg",
                          name: "Luggage",
                          page_description: "Shop Men's Wheeled Luggage. Versatile, rugged suitcases, baggage, holdalls and shoulder bags. All with famous long-lasting quality.",
                          page_title: "Men's Wheeled Luggage",
                          parent_category_id: "mens-accessories",
                          c_showInMenu: true
                      }
                  ],
                  id: "mens-accessories",
                  name: "Accessories",
                  page_description: "Shop mens accessories including belts, wallets. gloves, hats, watches, luggage & more.",
                  page_title: "Men's Accessories Belts, Wallets. Gloves, Hats, Watches, Luggage & More",
                  parent_category_id: "mens",
                  c_showInMenu: true
              }
          ],
          id: "mens",
          name: "Mens",
          page_description: "Men's range. Hard-wearing boots, jackets and clothing for unbeatable comfort day in, day out. Practical, easy-to-wear styles wherever you're headed.",
          page_title: "Men's Footwear, Outerwear, Clothing & Accessories",
          parent_category_id: "root",
          c_showInMenu: true
});

cat.save(function (err, result) {
    mongoose.disconnect();
});
